"""
saddle
Copyright (C) 2018  Kevin Cianfarini

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from django.contrib import admin
from music.models import SourceDirectory, Track, Playlist, PlaylistEntry

# Register your models here.

class SourceDirectoryAdmin(admin.ModelAdmin):
    model = SourceDirectory


admin.site.register(SourceDirectory, SourceDirectoryAdmin)


class TrackAdmin(admin.ModelAdmin):
    model = Track

admin.site.register(Track, TrackAdmin)


class PlaylistAdmin(admin.ModelAdmin):
    model = Playlist

admin.site.register(Playlist, PlaylistAdmin)


class PlaylistEntryAdmin(admin.ModelAdmin):
    model = PlaylistEntry

admin.site.register(PlaylistEntry, PlaylistEntryAdmin)