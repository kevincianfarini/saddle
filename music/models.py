"""
saddle
Copyright (C) 2018  Kevin Cianfarini

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from django.db import models

# Create your models here.


class Artist(models.Model):
    name = models.CharField(max_length=128, null=False, blank=False, unique=True)
    last_modified = models.DateTimeField(auto_now=True)


class Album(models.Model):
    name = models.CharField(max_length=128, null=True, blank=False)
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE, related_name='albums', null=True)
    last_modified = models.DateTimeField(auto_now=True)


    def __str__(self):
        return self.name

    class Meta:
        unique_together = ('name', 'artist')


class Track(models.Model):
    name = models.CharField(max_length=512)
    track_number = models.PositiveIntegerField(null=True, blank=True)
    artist = models.CharField(max_length=512)
    album = models.ForeignKey(Album, on_delete=models.CASCADE, related_name='tracks', null=True)
    backing_file = models.CharField(max_length=512, null=False, blank=False, unique=True)
    duration = models.PositiveIntegerField(null=False)
    mime_type = models.CharField(max_length=16)
    last_modified = models.DateTimeField(auto_now=True)


    def __str__(self):
        return '%d : %s -> (%s, %s)' % (self.track_number, self.name, self.artist, self.album.name)


class Playlist(models.Model):
    name = models.CharField(max_length=128, null=False, blank=False)
    tracks = models.ManyToManyField(
        Track,
        through='PlaylistEntry',
        through_fields=('playlist', 'track')
    )
    last_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class PlaylistEntry(models.Model):
    playlist = models.ForeignKey(Playlist, on_delete=models.CASCADE)
    track = models.ForeignKey(Track, on_delete=models.CASCADE)
    position = models.PositiveIntegerField(default=1)
    last_modified = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('playlist', 'position')


class SourceDirectory(models.Model):
    directory = models.CharField(max_length=512, null=False, blank=False)

    def __str__(self):
        return self.directory

    class Meta:
        verbose_name = "Source Directories"