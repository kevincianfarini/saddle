"""
saddle
Copyright (C) 2018  Kevin Cianfarini

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from django.core.management.base import BaseCommand, CommandError
from django.core.management import call_command
from music.models import SourceDirectory, Artist, Album, Track
import os
import mutagen

class Command(BaseCommand):
    help = 'Scans specified source directories for music files'

    def handle(self, *args, **options):
        def load_tags(music_file):
            audio_file = mutagen.File(music_file, easy=True)

            if audio_file is None:
                return

            if audio_file.get('albumartist', None):
                artist, artist_created = Artist.objects.get_or_create(name=audio_file['albumartist'][0])
            elif audio_file.get('artist', None):
                artist, artist_created = Artist.objects.get_or_create(name=audio_file['artist'][0])
            else:
                artist, artist_created = None, False

            if audio_file.get('album', None):
                album, album_created = Album.objects.get_or_create(
                    name=audio_file['album'][0],
                    artist=artist
                )
            else:
                album, album_created = None, False

            try:
                info = audio_file.get('tracknumber', None)
                if info is None:
                    raise ValueError
                track_number = int(info[0])
            except ValueError:
                try:
                    track_number = int(audio_file['tracknumber'][0].split('/')[0])
                except:
                    track_number = None

            track, track_created = Track.objects.get_or_create(
                name=audio_file.get('title', ['Unknown'])[0],
                track_number=track_number,
                artist=audio_file.get('artist', ['Unknown'])[0],
                duration=audio_file.info.length,
                album=album,
                backing_file=music_file,
                mime_type=audio_file._mimes[0]
            )

            if track_created:
                print(music_file)
        
        for directory in SourceDirectory.objects.all():
            for root, subdirs, files in os.walk(os.path.join('/host', *directory.directory.split('/'))):
                for file_name in files:
                    load_tags(os.path.join(root, file_name))
    