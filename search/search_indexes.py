"""
saddle
Copyright (C) 2018  Kevin Cianfarini

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from haystack import indexes
from music.models import Artist, Album, Track, Playlist


class ArtistIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, model_attr='name')

    def get_model(self):
        return Artist

    def update_object(self, instance, using=None, **kwargs):
        album_index = AlbumIndex()
        for album in instance.albums.all():
            album_index.update_object(album, using=using)
        super().update_object(instance, using=using, **kwargs)

    def index_queryset(self, using=None):
        return self.get_model().objects.all()


class AlbumIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    name = indexes.CharField(model_attr='name')

    def update_object(self, instance, using=None, **kwargs):
        track_index = TrackIndex()
        for track in instance.tracks.all():
            track_index.update_object(track, using=using)
        super().update_object(instance, using=using, **kwargs)

    def get_model(self):
        return Album

    def index_queryset(self, using=None):
        return self.get_model().objects.all()

    
class TrackIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    name = indexes.CharField(model_attr='name')

    def get_model(self):
        return Track

    def index_queryset(self, using=None):
        return self.get_model().objects.all()


class PlaylistIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, model_attr='name')

    def get_model(self):
        return Playlist

    def index_queryset(self, using=None):
        return self.get_model().objects.all()