"""
saddle
Copyright (C) 2018  Kevin Cianfarini

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from django.shortcuts import render
from rest_framework.response import Response
from rest_framework import views, status, generics
from haystack.query import SearchQuerySet
from search.serializers import AggregateSerializer
from api.pagination import StandardPagination


class SearchAPIView(generics.ListAPIView):
    pagination_class = StandardPagination
    serializer_class = AggregateSerializer

    def get_queryset(self):
        return SearchQuerySet().filter(
            content=self.request.query_params['q']
        ).load_all()

    def get(self, request, *args, **kwargs):
        if request.query_params.get('q', '') == '':
            return Response(
                data={'error': "Query param 'q' must not be empty"},
                status=status.HTTP_400_BAD_REQUEST
            )
        return super().get(request, *args, **kwargs)