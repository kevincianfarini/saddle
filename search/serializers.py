"""
saddle
Copyright (C) 2018  Kevin Cianfarini

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from drf_haystack.serializers import HaystackSerializer, HaystackSerializerMixin
from rest_framework import serializers
from search.search_indexes import (
    ArtistIndex, AlbumIndex, TrackIndex, PlaylistIndex
)
from api.serializers import (
    ArtistSerializer, AlbumSerializer, TrackSerializer, PlaylistSerializer
)


class ArtistSearchSerializer(HaystackSerializerMixin, ArtistSerializer):
    data_type = serializers.SerializerMethodField()

    class Meta(ArtistSerializer.Meta):
        search_fields = ('text', )

    def get_data_type(self, obj):
        return 'artist'


class AlbumSearchSerializer(HaystackSerializerMixin, AlbumSerializer):
    data_type = serializers.SerializerMethodField()

    class Meta(AlbumSerializer.Meta):
        search_fields = ('text', 'name')

    def get_data_type(self, obj):
        return 'album'


class TrackSearchSerializer(HaystackSerializerMixin, TrackSerializer):
    data_type = serializers.SerializerMethodField()

    class Meta(TrackSerializer.Meta):
        search_fields = ('text', 'name')

    def get_data_type(self, obj):
        return 'track'


class PlaylistSearchSerializer(HaystackSerializerMixin, PlaylistSerializer):
    data_type = serializers.SerializerMethodField()

    class Meta(PlaylistSerializer.Meta):
        search_fields = ('text', )

    def get_data_type(self, obj):
        return "playlist"


class AggregateSerializer(HaystackSerializer):
    class Meta:
        serializers = {
            ArtistIndex: ArtistSearchSerializer,
            AlbumIndex: AlbumSearchSerializer,
            TrackIndex: TrackSearchSerializer,
            PlaylistIndex: PlaylistSearchSerializer
        }