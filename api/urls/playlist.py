"""
saddle
Copyright (C) 2018  Kevin Cianfarini

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from django.urls import path
from api.views import (
    PlaylistRetrieveUpdateDestroyAPIView,
    PlaylistListCreateAPIView,
    PlaylistEntryListCreateAPIView,
    PlaylistEntryRetrieveUpdateDestroyAPIView
)

urlpatterns = [
    path('<int:pk>/', PlaylistRetrieveUpdateDestroyAPIView.as_view()),
    path('<int:pk>/tracks/', PlaylistEntryListCreateAPIView.as_view()),
    path('<int:_>/tracks/<int:pk>/', PlaylistEntryRetrieveUpdateDestroyAPIView.as_view()),
    path('', PlaylistListCreateAPIView.as_view())
]