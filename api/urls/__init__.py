"""
saddle
Copyright (C) 2018  Kevin Cianfarini

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from django.urls import path, include
from api.urls import track as track_urls
from api.urls import artist as artist_urls
from api.urls import album as album_urls
from api.urls import playlist as playlist_urls
from api.views import ScanLibraryAPIView

urlpatterns = [
    path('tracks/', include(track_urls)),
    path('artists/', include(artist_urls)),
    path('albums/', include(album_urls)),
    path('playlists/', include(playlist_urls)),
    path('scan/', ScanLibraryAPIView.as_view()),
]