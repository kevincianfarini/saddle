"""
saddle
Copyright (C) 2018  Kevin Cianfarini

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from django.http import FileResponse
from django.shortcuts import get_object_or_404
from django.core.management import call_command
from rest_framework import generics, status, viewsets, mixins
from rest_framework.views import APIView
from rest_framework.response import Response
from music.models import (
    Track,
    Artist,
    Album,
    PlaylistEntry,
    Playlist
)
from api.serializers import (
    TrackSerializer,
    ArtistSerializer,
    AlbumSerializer,
    PlaylistEntryReadSerializer,
    PlaylistEntryWriteSerializer,
    PlaylistSerializer
)
from api.pagination import StandardPagination
from api.mixins import TagUpdateMixin
from transcode import AudioTranscode
import os
import mutagen
import datetime

def get_last_updated_filter(request):
    return datetime.datetime.fromtimestamp(
            int(request.query_params.get(
                'last_updated',
                # no param implies getting all records
                # so we fetch records from the UNIX epoch
                0
            )
        )
    )


class TrackListAPIView(generics.ListAPIView):
    serializer_class = TrackSerializer
    pagination_class = StandardPagination

    def get_queryset(self):
        return Track.objects.filter(
            last_modified__gte=get_last_updated_filter(self.request)
        ).order_by('name')


class ArtistListAPIView(generics.ListAPIView):
    queryset = Artist.objects.all().order_by('name')
    serializer_class = ArtistSerializer
    pagination_class = StandardPagination

    def get_queryset(self):
        return Artist.objects.filter(
            last_modified__gte=get_last_updated_filter(self.request)
        ).order_by('name')


class AlbumListAPIView(generics.ListAPIView):
    serializer_class = AlbumSerializer
    pagination_class = StandardPagination

    def get_queryset(self):
        return Album.objects.filter(
            last_modified__gte=get_last_updated_filter(self.request)
        )


class PlaylistListCreateAPIView(generics.ListCreateAPIView):
    serializer_class = PlaylistSerializer
    pagination_class = StandardPagination

    def get_queryset(self):
        return Playlist.objects.filter(
            last_modified__gte=get_last_updated_filter(self.request)
        )


class ArtistRetrieveUpdateAPIView(generics.RetrieveAPIView, TagUpdateMixin):
    serializer_class = ArtistSerializer
    queryset = Artist.objects.all()

    def coalesce(self, instance, serializer):
        if len(serializer.errors) == 1 and 'exists' in serializer.errors['name'][0]:
            coalesced_instance = Artist.objects.get(name=serializer.data['name'])
            Album.objects.filter(artist=instance).update(artist=coalesced_instance)
            instance.delete()
            return True, self.get_serializer(coalesced_instance)
        return False, serializer

    def update_file_tags(self, instance, name, **kwargs):
        tracks = Track.objects.filter(
            album__in=Album.objects.filter(
                artist=instance
            )
        )
        for track in tracks:
            audio_file = mutagen.File(track.backing_file, easy=True)
            audio_file['albumartist'] = name
            audio_file.save()

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)


class ArtistAlbumsAPIView(generics.ListAPIView):
    serializer_class = AlbumSerializer

    def get_queryset(self):
        return Album.objects.filter(
            artist__id=self.kwargs['pk']
        )


class AlbumCreateUpdateAPIView(generics.RetrieveAPIView, TagUpdateMixin):
    serializer_class = AlbumSerializer
    queryset = Album.objects.all()

    def coalesce(self, instance, serializer):
        if len(serializer.errors) == 1 and 'unique' in serializer.errors['non_field_errors'][0]:
            coalesced_instance = Album.objects.get(name=serializer.data['name'])
            Track.objects.filter(album=instance).update(album=coalesced_instance)
            instance.delete()
            return True, self.get_serializer(coalesced_instance)
        return False, serializer

    def update_file_tags(self, instance, name, **kwargs):
        tracks = Track.objects.filter(
            album=instance
        )
        for track in tracks:
            audio_file = mutagen.File(track.backing_file, easy=True)
            audio_file['album'] = name
            audio_file.save()

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)


class AlbumTracksAPIView(generics.ListAPIView):
    serializer_class = TrackSerializer

    def get_queryset(self):
        return Track.objects.filter(
            album__id=self.kwargs['pk']
        )


class TrackAPIView(generics.GenericAPIView, TagUpdateMixin):
    serializer_class = TrackSerializer
    queryset = Track.objects.all()

    def get(self, request, *args, **kwargs):
        track = get_object_or_404(Track, pk=self.kwargs.get('pk', -1))
        bitrate = request.query_params.get('bitrate', None)
        if bitrate is not None and bitrate not in ['128', '256', '320']:
            return Response(
                {'error': "'bitrate' param must be [128, 256, 320]"},
                status=status.HTTP_400_BAD_REQUEST
            )

        audio_file, mime_type, file_length = self.get_stream_info(
            track=track,
            bitrate=bitrate
        )

        response = FileResponse(
            audio_file,
            content_type=mime_type,
        )
        response['Content-Length'] = file_length
        return response

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def update_file_tags(self, instance, name, track_number, artist, **kwargs):
        audio_file = mutagen.File(instance.backing_file, easy=True)
        audio_file['title'] = name
        audio_file['artist'] = artist
        audio_file['tracknumber'] = [track_number]
        audio_file.save()

    def coalesce(self, *args, **kwargs):
        raise ValueError('Tracks should never attempt to coalesce') 

    def get_stream_info(self, track, bitrate):
        """
        Returns a tuple of (file, mime_type, file_size)
        """
        f_bitrate = int(mutagen.File(track.backing_file).info.bitrate / 1000)
        
        if bitrate is None or f_bitrate < int(bitrate):
            return (
                open(track.backing_file, 'rb'),
                track.mime_type,
                os.path.getsize(track.backing_file)
            )

        return (
            AudioTranscode().transcode_stream(track.backing_file, 'mp3', bitrate=int(bitrate)),
            'audio/mpeg',
            track.duration * 125 * int(bitrate)
        )


class PlaylistRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PlaylistSerializer
    queryset = Playlist.objects.all()


class PlaylistEntryListCreateAPIView(generics.ListCreateAPIView):
    pagination_class = StandardPagination

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return PlaylistEntryReadSerializer
        return PlaylistEntryWriteSerializer
        
    def get_queryset(self):
        return PlaylistEntry.objects.filter(
            playlist__id=self.kwargs['pk']
        )


class PlaylistEntryRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PlaylistEntryWriteSerializer
    queryset = PlaylistEntry.objects.all()


class ScanLibraryAPIView(generics.GenericAPIView):
    def get(self, request, *args, **kwargs):
        call_command('scan')
        return Response(
            status=status.HTTP_200_OK
        )
