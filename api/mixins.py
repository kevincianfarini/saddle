"""
saddle
Copyright (C) 2018  Kevin Cianfarini

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from rest_framework.response import Response
from rest_framework import status


class TagUpdateMixin(object):    
    def update_file_tags(self, instance, **data):
        raise NotImplementedError

    def coalesce(self, instance, serializer):
        """
        This method resolves if an instance being updated
        should be coalesced with another instance in the database.
        If it did, return True along with the serializer of the coalesced instance
        if not, return false and the invalid serializer
        """
        raise NotImplementedError

    def update(self, request, *args, **kwargs):
        """
        We opt not to use mixins.UpdateModelMixin to add
        support for coalescing and access to the validated 
        serializer data
        """
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=False)

        if not serializer.is_valid():
            coalesced, serializer = self.coalesce(instance, serializer)
            if not coalesced:
                return Response(
                    data=serializer.errors,
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            serializer.save()
        self.update_file_tags(serializer.instance, **serializer.data)
        return Response(serializer.data)