"""
saddle
Copyright (C) 2018  Kevin Cianfarini

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator
from music.models import Track, Artist, Album, Playlist, PlaylistEntry


class ArtistSerializer(serializers.ModelSerializer):

    class Meta:
        exclude = ['last_modified', ]
        model = Artist


class AlbumSerializer(serializers.ModelSerializer):
    artist = ArtistSerializer(read_only=True)
    duration = serializers.SerializerMethodField()

    class Meta:
        exclude = ['last_modified', ]
        model = Album
        validators = [
            UniqueTogetherValidator(
                queryset = Album.objects.all(),
                fields=('artist', 'name')
            )
        ]

    def get_duration(self, obj):
        return sum(map(lambda track: track.duration, obj.tracks.all()))


class PlaylistSerializer(serializers.ModelSerializer):

    class Meta:
        exclude = ['tracks', 'last_modified']
        model = Playlist


class TrackSerializer(serializers.ModelSerializer):
    album = AlbumSerializer(read_only=True)
    duration = serializers.IntegerField(read_only=True)

    class Meta:
        exclude = ['backing_file', 'mime_type', 'last_modified']
        model = Track


class PlaylistEntryReadSerializer(serializers.ModelSerializer):
    track = TrackSerializer(read_only=True)

    class Meta:
        exclude = ['playlist', 'last_modified']
        model = PlaylistEntry


class PlaylistEntryWriteSerializer(serializers.ModelSerializer):
    track = serializers.PrimaryKeyRelatedField(queryset=Track.objects.all())
    playlist = serializers.PrimaryKeyRelatedField(queryset=Playlist.objects.all())

    class Meta:
        exclude = ['last_modified', ]
        model = PlaylistEntry