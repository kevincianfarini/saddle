FROM python:3.6.6-stretch
RUN set -ex; apt-get update; apt-get install -y software-properties-common; add-apt-repository -y non-free; apt-get update; apt-get -y install flac ffmpeg lame vorbis-tools faac faad
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
ADD . /code/
RUN mkdir /host